<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_project_rw1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ee<q09<R=yx| *?S/77RHU736RGcZ6 MP2duFJSK]_27D}!N6ysSBN=3SzJd_i.f');
define('SECURE_AUTH_KEY',  '}&u,,mQXwepw[{xiV/Wh1H;RSak0{cu#DeGjx4>$eh3ZaAQE_/s6^!RO&:?da;}o');
define('LOGGED_IN_KEY',    'sJ(LsWR @YHs8mCy:XC?w|49iC$M*SM&]X2I&AoFcjv`%:w;`d<`l82NHv,xU0V.');
define('NONCE_KEY',        'g:axUcFr%4s0jF1b!LKt,A&q[ZbIAUEePZ-A)7`PCB76BXS:2|?w)*sCy&+<XgLg');
define('AUTH_SALT',        'Rc8Ne>d4$*-~MuWB79V7vNiV&m%R^Q-U1Dg50,6Yai`!t3/x4E>q2`_xh3hsNf7)');
define('SECURE_AUTH_SALT', '8k+CX/kJvnk_c3GD?L86iJOlg|QT(>s`uN3&6Z D5|V5f&pHaj:eKb*^q$HF%t2!');
define('LOGGED_IN_SALT',   'ivHbF1&3l%L,m;c<P-Uz/)[07/.Mq+Ay@KpR@D4XwE|bZYD^oa,d&mLmBZOZ!pO6');
define('NONCE_SALT',       '^?FH1,)}&8 H[NhQtlg.X)8~-;VYjQQRc^f,dh9doA|7`l,&L@(#RyBI_MXAq>!>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'rw1';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
